import axios from "axios";

export const fetchData = async (
  actionVal: string,
  hasParams?: boolean,
  params?: { [key: string]: any }
) => {
  try {
    const res = await axios.get("https://safecare-staging.s10health.com/api", {
      params: {
        actionVal,
        module: "Support",
        ...(hasParams && { ...params }),
      },
    });
    return res.data;
  } catch (error) {
    return Promise.reject(error);
  }
};
