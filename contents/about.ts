import { HandKeys, KeyBunch, OpenBook, ToDo } from "../SVGs";
import { SVG } from "../SVGs/customerSupport";

export const contents = {
  header: {
    one: "Customer Insight,",
    two: "Professional Support",
  },
  description: `Semaj Africa is an online education platform that delivers video courses, programs and resources for Individual, Advertising & Media Specialist, Online Marketing Professionals, Freelancers and anyone looking to pursue a career in digital marketing, Accounting, Web development, Programming. Multimedia and CAD design.`,
  iconList: [ToDo(), KeyBunch(), OpenBook(), HandKeys()],
  mainSvg: SVG(),
};
