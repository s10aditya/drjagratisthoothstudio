import {
  Person,
  Hospital,
  Mail,
  Phone,
  PersonGlasses,
  Doctor,
} from "../SVGs/bookingForm";

export const contents = {
  header: "Consult your doctor",
  tabItems: [
    { label: "Online Consulting", value: "online consulting" },
    { label: "Clinical Appointment", value: "clinical appointment" },
  ],
  filterItems: [{ label: "All Doctors", value: "all doctors" }],
  filterSearch: "Search",
  searchPlaceholder: "Doctor name",
  filter: {
    heading: "Consult Type",
    buttons: ["Paid", "Free"],
    rangeHeading: "Price",
  },
  clinical: {
    booking: {
      header: "Book your appointment",
      description: "We will confirm your appointment within 2 hours",
    },
    timings: {
      header: "Working Hours",
      list: [
        "Monday       : 9:00 am - 6:00 pm",
        "Tuesday       : 9:00 am - 6:00 pm",
        "Wednesday : 9:00 am - 6:00 pm",
        "Thursday     : 9:00 am - 6:00 pm",
        "Friday           : 9:00 am - 6:00 pm",
        "Saturday      : 9:00 am - 6:00 pm",
        "Sunday         : 9:00 am - 6:00 pm",
      ],
    },
  },
  bookingForm: [
    {
      label: "Name",
      name: "name",
      placeholder: "Enter your name",
      icon: Person(),
      type: "text-field",
    },
    {
      label: "Email",
      name: "emailId",
      placeholder: "Enter your email",
      icon: Mail(),
      type: "text-field",
    },
    {
      label: "Phone",
      name: "phone",
      placeholder: "Enter your phone number",
      icon: Phone(),
      type: "text-field",
    },
    {
      label: "Services",
      name: "services",
      placeholder: "",
      icon: Hospital(),
      type: "select-field",
    },
    {
      label: "Doctor",
      name: "doctor",
      placeholder: "Choose your doctor",
      icon: Doctor(),
      type: "select-field",
    },
    {
      label: "Age",
      name: "age",
      placeholder: "Enter your age",
      icon: PersonGlasses(),
      type: "text-field",
    },
  ],
};
