import { FB, Twitter, YT } from "../SVGs/SocialMedias";

export const contents = {
  sampleBlog: {
    imageOne: {
      src: "/Blog/blog-default.png",
      alt: "girl-holding-probiotics",
      width: "1101px",
      height: "534px",
    },
    description: {
      one: `Luxury is something everyone deserves from time to time. Such an indulgence can make a vacation a truly rejuvenating experience. One of the best ways to get the luxury of the rich and famous to fit into your budget can be yours through yacht charter companies. These companies specialize in creating custom sailing vacations that redefine travel. 

      Planning Your Luxury Trip 
      
      With your budget in mind, it is easy to plan a chartered yacht vacation. Companies often have a fleet of sailing vessels that can accommodate parties of various sizes. You may want to make it a more intimate trip with only close family. There are charters that can be rented for as few as two people. These include either a sailboat or motorboat and can come with or without a crew and captain to sail the ship for you. If you choose not to hire a crew, you will have to show that you are knowledgeable of sailing and can handle the ship competently.
      
      The next part of planning is to determine your starting and ending ports. This could be a place close to home and sail in one area or start and finish at two different ports. Generally, starting and stopping in the same port will save you money and is usually more convenient. You can also fly to a destination far from home and then sail another exotic sea. There are luxury yacht charter companies that cruise the Caribbean and Mediterranean seas or around Alaska, the Panama Canal, or anyplace you can imagine.
      
      Determining the type of cruise is another aspect of planning a chartered yachting trip. You can have as little or many crew members as the ship will hold. A captain takes all the worries out of navigating and onboard housekeeping services make it a real vacation that rivals the finest hotel services. You can also choose to have a chef and service crew as part of your vacation package.`,
      two: `Luxury is something everyone deserves from time to time. Such an indulgence can make a vacation a truly rejuvenating experience. One of the best ways to get the luxury of the rich and famous to fit into your budget can be yours through yacht charter companies. These companies specialize in creating custom sailing vacations that redefine travel. 

      Planning Your Luxury Trip 
      
      With your budget in mind, it is easy to plan a chartered yacht vacation. Companies often have a fleet of sailing vessels that can accommodate parties of various sizes. You may want to make it a more intimate trip with only close family. There are charters that can be rented for as few as two people.`,
    },
    subHeading: `Another Sub-heading Information
        will be a perfect fit here.`,
    imageTwo: {
      src: "/Blog/blog-default-2.png",
      alt: "probiotic",
      width: "896px",
      height: "444px",
    },
    tags: { header: "Tags: ", list: ["Technology", "Design", "Computer"] },
  },
  social: {
    header: "Share with",
    list: [
      { icon: FB({ size: { width: 44, height: 45 } }), link: "/" },
      { icon: Twitter({ size: { width: 45, height: 45 } }), link: "/" },
      { icon: YT({ size: { width: 45, height: 45 } }), link: "/" },
    ],
  },
  otherBlogs: {
    prev: {
      cap: "Prev post",
      title: "9 Marketing Techniques in 2019",
    },
    next: {
      cap: "Next post",
      title: "Learn  What Makes A Successful Business",
    },
  },
  comments: {
    header: "Comment",
    count: 4,
    sampleComment: {
      description: `“With your budget in mind, it is easy to plan a chartered yacht vacation. Companies often have a fleet of sailing vessels that can accommodate parties of various sizes.”`,
      date: "35 mins ago, 15 November 2019",
    },
    comment: {
      header: "Leave a comment",
      description:
        "Your email address will not be published. Required fields are marked *",
    },
  },
};
