export const contents = {
  visualStories: {
    header: "Visual Stories",
    stories: [
      {
        image: { src: "/", alt: "story-image", width: 216, height: 263 },
        title: `Become A Social Media Expert`,
      },
      {
        image: { src: "/", alt: "story-image", width: 216, height: 263 },
        title: `Become A Social Media Expert`,
      },
      {
        image: { src: "/", alt: "story-image", width: 216, height: 263 },
        title: `Become A Social Media Expert`,
      },
      {
        image: { src: "/", alt: "story-image", width: 216, height: 263 },
        title: `Become A Social Media Expert`,
      },
      {
        image: { src: "/", alt: "story-image", width: 216, height: 263 },
        title: `Become A Social Media Expert`,
      },
    ],
  },
  blogCategories: {
    header: "Blog Categories",
    categories: [
      { label: "All", value: "all" },
      { label: "Dental", value: "dental" },
      { label: "Diabetes", value: "diabetes" },
      { label: "Eye", value: "eye" },
      { label: "Skin", value: "skin" },
      { label: "Hair", value: "hair" },
    ],
    blogs: [
      {
        image: {
          src: "/",
          alt: "blog-image",
          width: 286,
          height: 220,
        },
        title: `Become A Social Media Expert`,
      },
      {
        image: {
          src: "/",
          alt: "blog-image",
          width: 286,
          height: 220,
        },
        title: `Advance Your 3d Modelling Skill`,
      },
      {
        image: {
          src: "/",
          alt: "blog-image",
          width: 286,
          height: 220,
        },
        title: `The Art Of Growing Relationship`,
      },
      {
        image: {
          src: "/",
          alt: "blog-image",
          width: 286,
          height: 220,
        },
        title: `Skills Needed In Becoming A Designer`,
      },
      {
        image: {
          src: "/",
          alt: "blog-image",
          width: 286,
          height: 220,
        },
        title: `Get Weekly Shopping Tips From Tesha`,
      },
      {
        image: {
          src: "/",
          alt: "blog-image",
          width: 286,
          height: 220,
        },
        title: `Learn Logo Design`,
      },
      {
        image: {
          src: "/",
          alt: "blog-image",
          width: 286,
          height: 220,
        },
        title: `Become A Web Developer`,
      },
      {
        image: {
          src: "/",
          alt: "blog-image",
          width: 286,
          height: 220,
        },
        title: `Embark On The Journey Of Becoming An Artist `,
      },
    ],
  },
};
