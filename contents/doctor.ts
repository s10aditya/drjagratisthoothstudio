import {
  FBOutlined,
  InstaOutlined,
  TwitterOutlined,
} from "../SVGs/SocialMedias";

import { Award1, Award2, Award3, Award4 } from "../SVGs/doctorAwards";

export const contents = {
  header: "Doctors name",
  qualifications: ["Dermatology", "MBBS MD DVL"],
  clinicName: `Dr Lavanya's skin clinic`,
  clinicAddress: "Lakshmipuram, Mysore - 570008",
  rightButtons: ["Clinical appointment", "$ 300"],
  socialInterest: {
    text: " People have shown interest in this Doctor",
    peopleInterested: 1111,
  },
  sampleDoctorSmall: {
    src: "/Home/doctor-one.jpg",
    width: 300,
    height: 300,
    alt: "doctor-smiling",
  },
  sampleDoctor: {
    imageOne: {
      src: "/Home/doctor-one.jpg",
      width: 339,
      height: 250,
      alt: "doctor-smiling",
    },
    imageTwo: {
      src: "/Home/doctor-two.jpg",
      width: 339,
      height: 250,
      alt: "doctor-smiling",
    },
    imageThree: {
      src: "/Home/doctor-three.jpg",
      width: 339,
      height: 250,
      alt: "doctor-smiling",
    },
    imageFour: {
      src: "/Home/doctor-four.jpg",
      width: 339,
      height: 250,
      alt: "doctor-smiling",
    },
    imageFive: {
      src: "/Home/doctor-five.jpg",
      width: 339,
      height: 250,
      alt: "doctor-smiling",
    },
    imageSix: {
      src: "/Home/doctor-six.jpg",
      width: 339,
      height: 250,
      alt: "doctor-smiling",
    },
    headerOne: "Dr. Jagrati Agarwal",
    headerTwo: "Dr. Ieahwaryah",
    headerThree: "Dr. Sowmiya",
    headerFour: "Dr. Dhivya",
    headerFive: "Dr. Reeja",
    headerSix: "Dr. Vineesha",
  },
  socialMedias: [
    FBOutlined({ size: { width: 27, height: 25 } }),
    InstaOutlined({ size: { width: 30, height: 31 } }),
    TwitterOutlined({ size: { width: 33, height: 33 } }),
  ],
  timeSlots: {
    header: "Time slots",
    availableTimeSlots: [
      "9:00",
      "9:00",
      "9:00",
      "9:00",
      "9:00",
      "9:00",
      "9:00",
    ],
    am: "AM:",
    pm: "PM:",
  },
  about: {
    sectionOne: {
      header: "About this doctor",
      description: `Ever wondered how other UX designers troubleshoot problems and juggle conflicting priorities? In this weekly series, Drew Bridewell—a user experience designer and leader of the digital transformation team at InVision—shares his hard-earned knowledge and shows how to apply basic UX design principles to real-world projects.  of the digital transformation team at InVision—shares his hard-earned knowledge and shows how to apply basic UX design principles to real-world projects. Ever wondered how other UX designers troubleshoot problems and juggle conflicting priorities? In this weekly series, Drew Bridewell—a user experience designer and leader of the digital transformation team at InVision—shares his hard-earned knowledge and shows how to apply basic UX design principles to real-world projects.  of the digital transformation team at InVision—shares his hard-earned knowledge and shows how to apply basic UX design principles to real-world projects. Ever wondered how other UX designers troubleshoot problems and juggle conflicting priorities? In this weekly series, Drew Bridewell—a user experience designer and leader of the digital transformation team at InVision—shares his hard-earned knowledge and shows how to apply basic UX design principles to real-world projects.  of the digital transformation team at InVision—shares his hard-earned knowledge and shows how to apply basic UX design principles to real-world projects. `,
    },
    sectionTwo: {
      header: "Awards",
      awards: [
        { svg: Award1, text1: "5,679", label: "Registered Students" },
        {
          svg: Award2,
          text1: "2,679",
          label: "Student has been helped to achieve their dreams",
        },
        {
          svg: Award3,
          text1: "10,000",
          label: "More than 10,000 people visits our site monthly",
        },
        {
          svg: Award4,
          text1: "#10",
          label:
            "Ranked among the top 10 growing online learning startups in West Africa",
        },
      ],
      description: `Lorem ipsum dolor sit amet, consectetur adipiscing sed do eiusmod tempor incididunt labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident,`,
    },
  },
};
