export const heroCarousel = {
  header: {
    one: "Grow",
    two: "with S10.Clinic",
    three: "Try better technology for better care",
  },
  button: {
    text: "Consult your doctor",
  },
  image: {
    src: "/Home/Hero.png",
    width: 640,
    height: 455,
    alt: "welcoming-doctors",
  },
};

export const masonryImages = [
  {
    src: "/Home/blogOne.jpg",
    alt: "flower-pot",
    width: 159,
    height: 220,
  },
  {
    src: "/Home/flower-pot.png",
    alt: "flower-pot",

    width: 100,
    height: 140,
  },
  {
    src: "/Home/masonry-item-7.png",
    alt: "masonry-item",

    width: 330,
    height: 300,
  },
  {
    src: "/Home/flower-pot.png",
    alt: "flower-pot",

    width: 204,
    height: 281,
  },
  {
    src: "/Home/masonry-item-5.png",
    alt: "masonry-item",

    width: 254,
    height: 280,
  },
  {
    src: "/Home/flower-pot.png",
    alt: "flower-pot",

    width: 100,
    height: 140,
  },
  {
    src: "/Home/masonry-item-7.png",
    alt: "masonry-item",

    width: 280,
    height: 220,
  },
];

export const aboutUs = {
  headerOne: "About Us",
  headerTwo: "Maxillofacial Prosthodontics and Implantology",
  headerThree: "Periodontics",
  description: `Dr. Jagrati Agarwal, the chief dentist at Dr. Jagrati's Tooth Studio is a Maxillofacial Prosthodontist and Implantologist. She persued her undergraduation and post- graduation from KLE Dental College,  Rajiv Gandhi University of Health Sciences, Bangalore.
  With over 10 years of experience as a clinical practitioner, she excels in her field of specialisation. She has presented papers and won awards at various state and national level conferences.`,
  descriptionTwo: `Dr. Jagrati believes each individual is unique and that their smiles are characteristic of their personality and should be given utmost care. She strives to achieve patient satisfaction at all times and works with complete sincerity.
  Her prime area of interest is esthetic prosthesis and restorations.
  As an individual, she is very approachable and friendly. Patients of all age groups share a good doctor-patient relationship with her, something she belives is very essential. `,
  descriptionThree: `Dr. Sowmiya Jayapal- Periodontist and Laser Specialist Dr. Sowmiya completed BDS in 2012, MDS Periodontics in 2016 from Sri Ramachandra University, Chennai.She practises Laser dentistry and incorporates the same in all her Surgical procedures. She is excellent at Gum surgery, Laser curettage, smile designing  Endo-Perio Surgery and patient awareness. 
  With a  extensive experience in these fields, she excells in her work and strives for perfection at all times.She follows the Mantra: Early diagnosis and Treatment  prevents  Early tooth loss at young age.`,
  headerFour: `Oral and Maxillofacial Surgery`,
  decriptionFour: `Dr. Dhivya Sridhar persued her Undergraduate and postgraduate degrees at Sri Ramachandra University, Chennai and has been practicing In the city as a consultant ever since.
  Apart from surgical extractions, Dr. Dhivya also specializes in minor and major surgeries associated with trauma and carcinomas. 
  She is a gentle and pragmatic dental surgeon amd has kept herself updated with the latest advancements in her field. She has a wide experience and has treated patients of varied age groups. 
  Her focus is always on providing her patients a pain free and smile filled experience.`,
  headerFive: `Endodontics and Restorative Dentistry`,
  descriptionFive: `Dr. Reeja graduaed from Rajas Dental College, Chennai and completed her post-graduation in Endodontics and Restorative Dentistry from Aadhiparasakthi Dental College, Chennai.
  With over 17 years of clinical experience and 3 years of teaching experience, Dr. Reeja excells in handling complicated cases. She is an astounding dentist who is skilled in esthetic restorations, smile designing amd single visit root canal treatment. 
  She is meticulous at her work, gentle with patients, communicates well and is a patient listener making her patients feel comfortable when she is treating them.`,
  headerSix: `Orthodontics and Dentofacial Orthopedics`,
  descriptionSix: `Dr. Vineesha Balachander pursued her undergraduation and her Postgraduation in the field of Orthodontics and dental orthopaedics from TN Dr.MGR Medical University. 
  She specialises in Invisible braces and is a trained Platinum Invisalign (USA) Specialist in Chennai.
  Dr. Vineesha is highly skilled and has a treated a variety of cases for misaligned teeth.
  She is cheerful and friendly and believes that correct allignment of teeth is essential for the overall well being of the patient. `,
  headerSeven: `Paedodontics and Preventive Dentistry`,
  descriptionSeven: `Dr. Ieshwaryeah persued her Course of Paedodontics and Preventive Dentistry from Ramachandra University, Chennai. She is an experienced clinical practitioner and has treated many children for a multitude of dental issues. She specialises in restorations, extractions, child orthodontics and preventive care.
  She is excellent at building a friendly bond with children thus alleviating their anxiety and making their dental treatment a happy experience.`,
  image: {
    // src: "/Home/asian-stock.png",
    src: "/Home/test1.png",
    alt: "asian-stock",
    width: 1000,
    height: 556,
  },
  button: {
    text: "Discover",
    styleType: "gradientBackground",
  },
};

export const consultBlock = {
  headerOne: "Our Qualified Doctors",
  button: { text: "View more" },
};

export const consultCard = {
  image: {
    src: "/Home/doctor-smiling.png",
    width: 339,
    height: 250,
    alt: "doctor-smiling",
  },
  headerOne: "Doctor name",
  description: {
    one: "Dermatology",
    two: "MBBS MD",
  },
  button: {
    text: "Book now",
    styleType: "gradientBackground",
  },
};

export const contactUs = {
  headerOne: "Contact us",
  description: `Feel free to contact us any time. We will get back to you as soon as
  we can!`,
  button: {
    text: "Send",
  },
  info: {
    header: "Info",
    phone: "+91-87544 71488",
    address: `156, TT Krishnamachari Road, Royapettah,Opposite to Savera hotel back gate, Chennai,
    Tamil Nadu 600014`,
  },
};

export const ourBlogs = {
  header: "Our Blogs",
  description: {
    one: "Visual Stories:",
    two: `A lot has been said recently about
    telemedicine in India. The fact of the matter is Virtual consultations
    is a more convenient and gives easy accessible healthcare for patients.
    The health seeker doesn’t have to travel long distances for an
    appointment. They can directly talk with the doctor and get their
    concerns easily addressed in the comfort of their home.`,
  },
  button: "Explore more",
};
