export const contents = {
  header: "Medicine Order",
  sidebar: {
    items: [
      {
        title: "Category",
        content: [
          "Category 1",
          "Category 2",
          "Category 3",
          "Category 4",
          "Category 5",
        ],
      },
      {
        title: "Brand",
        content: ["Brand 1", "Brand 2", "Brand 3", "Brand 4", "Brand 5"],
      },
      { title: "Ratings", content: [5, 4, 3, 2, 1] },
    ],
    other: {
      title: "Price",
      content: ["Min", "Max"],
      buttons: [
        { text: "Apply", styleType: "gradientBackground", size: "small" },
        { text: "Reset", styleType: "outlined", size: "small" },
      ],
    },
  },
};
