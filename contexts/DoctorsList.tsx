import { useReducer, useContext, createContext, ReactNode } from "react";

type ReducerAction = {
  type: string;
  payload: any;
};

const DoctorsListSetter = createContext((p: ReducerAction) => {});
const DoctorsListGetter = createContext([]);

const reducer = (_: null, action: ReducerAction) => action.payload;

export const DoctorsListProvider = ({ children }: { children: ReactNode }) => {
  const [state, dispatch] = useReducer(reducer, []);
  return (
    <DoctorsListSetter.Provider value={dispatch}>
      <DoctorsListGetter.Provider value={state}>
        {children}
      </DoctorsListGetter.Provider>
    </DoctorsListSetter.Provider>
  );
};

export const useDoctorsList = () => useContext(DoctorsListGetter);
export const useDispatchDoctorsList = () => useContext(DoctorsListSetter);
