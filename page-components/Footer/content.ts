import { FB, Dribble, Twitter, YT } from "../../SVGs/SocialMedias";

export const Contents = {
  logo: {
    src: "/Logos/jagrati-logo.png",
    alt: "jagrati-logo",
    width: 60,
    height: 60,
  },
  social: [
    { icon: FB({ size: { width: 44, height: 45 } }), link: "/" },
    { icon: Dribble({ size: { width: 44, height: 45 } }), link: "/" },
    { icon: Twitter({ size: { width: 45, height: 45 } }), link: "/" },
    { icon: YT({ size: { width: 45, height: 45 } }), link: "/" },
  ],
  ourLinks: {
    header: "Our Links",
    links: [
      {
        label: "Home",
        link: "/",
      },
      // {
      //   label: "Appointments",
      //   link: "/",
      // },
      // {
      //   label: "Blog",
      //   link: "/",
      // },
      {
        label: "About Us",
        link: "/",
      },
      {
        label: "Contact Us",
        link: "/",
      },
    ],
  },
  ourFeatures: {
    header: "Our Features",
    items: [
      // "Online consulting",
      "Book appointment",
      // "Choose plan",
      "Ask Dr Bot",
      "Medicine order",
    ],
  },
  subscribe: {
    header: "Our Support",
    button: "Submit",
    final: {
      one: "Terms and Conditions",
      two: "Support",
      three: "FAQ",
    },
  },
  credits: {
    rights: "All Right Reserved | S10.Clinic 2021",
    policy: "Privacy policy",
    siteCredit: "Site Credit",
  },
};
