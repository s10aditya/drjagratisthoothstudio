import { FC } from "react";
import styles from "./Footer.module.scss";
import Image from "next/image";
import { Contents } from "./content";
import { uid } from "react-uid";

export const Footer: FC = () => (
  <>
    <footer className={styles.footer__main}>
      <div className={styles.footer__blockOne}>
        {/* eslint-disable-next-line jsx-a11y/alt-text */}
        <Image {...Contents.logo} />
        <div className={styles.footer__socialItems}>
          {Contents.social.map((item, ind) => (
            <span key={uid(ind)}>{item.icon}</span>
          ))}
        </div>
      </div>
      <div className={styles.footer__blockTwo}>
        <h6 style={{ marginLeft: "50px" }}>{Contents.ourLinks.header}</h6>
        <div className={styles.footer__linkItems}>
          {Contents.ourLinks.links.map((item, ind) => (
            <div key={uid(ind)} className={styles.footer__linkItem}>
              {item.label}
            </div>
          ))}
        </div>
      </div>
    </footer>
    <div className={styles.footer__credits}>
      <p>{Contents.credits.rights}</p>
    </div>
  </>
);
