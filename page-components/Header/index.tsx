import { FC } from "react";
import Link from "next/link";
import Image from "next/image";
import styles from "./Header.module.scss";
import { uid } from "react-uid";
import SearchBar from "../Searchbar/SearchBar";
import { NavItems } from "../../contents/header";
import { AddToCartIcon } from "../../SVGs";
import clsx from "clsx";
import { useRouter } from "next/router";
import { ClinicLogo } from "../../contents/common";

export const Header: FC = () => {
  const router = useRouter();

  return (
    <header className={styles.header__main}>
      <div className={styles.header__logo}>
        <div className={styles.header__main__cart}>
          <Image {...ClinicLogo} />
        </div>
      </div>
      <div className={styles.header__navItemsWrapper}>
        <div className={styles.header__main__cart}>
          <h5>Dr Jagrati's Tooth Studio</h5>
        </div>
      </div>

      <div className={styles.header__main__end}>
        <SearchBar />
      </div>
    </header>
  );
};
