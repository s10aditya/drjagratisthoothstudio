import { FC } from "react";
import { SearchIcon } from "../../SVGs";
import styles from "./SearchBar.module.scss";

const SearchBar: FC = () => {
  return (
    <div className={styles.searchBar__main}>
      <input className={styles.searchBar__input} placeholder="Search" />
      <SearchIcon size={13} color="#d9d9d9" />
    </div>
  );
};

export default SearchBar;
