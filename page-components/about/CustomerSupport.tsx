import { FC } from "react";
import { uid } from "react-uid";
import { contents } from "../../contents/about";
import styles from "./CustomerSupport.module.scss";

export const CustomerSupport: FC = () => (
  <section className={styles.customerSupport__main}>
    <div className={styles.customerSupport__childOne}>
      <h4>
        <span>{contents.header.one}</span>
        <span>{contents.header.two}</span>
      </h4>
      <p>{contents.description}</p>
      <div>
        {contents.iconList.map((item, ind) => (
          <span key={uid(ind)}>{item}</span>
        ))}
      </div>
    </div>
    <div>{contents.mainSvg}</div>
  </section>
);
