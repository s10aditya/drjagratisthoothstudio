import { Select, TextField } from "@mui/material";
import { FC } from "react";
import { Controller, useForm } from "react-hook-form";
import { uid } from "react-uid";
import S10Button from "../../reusable-components/Button";
import { contents } from "../../contents/appointment";
import { TBookingFormItem } from "../../types/pages/appointment";
import styles from "./BookingForm.module.scss";

const IconField = ({
  icon,
  placeholder,
  label,
  type,
  name,
  control,
  listKey,
}: TBookingFormItem) => (
  <div key={listKey} className={styles.iconField}>
    {icon}
    <div>
      <label>{label}</label>
      <Controller
        name={name}
        control={control}
        render={({ field }) =>
          type === "text-field" ? (
            <TextField
              {...field}
              placeholder={placeholder}
              variant="standard"
            />
          ) : (
            <Select {...field} variant="standard" />
          )
        }
      />
    </div>
  </div>
);

const BookingForm: FC = () => {
  const { control, handleSubmit } = useForm({ mode: "onBlur" });

  return (
    <form
      onSubmit={handleSubmit((v) => console.log(v))}
      className={styles.clinicForm}
    >
      <div className={styles.bookingForm__main}>
        {contents.bookingForm.map((item, ind) => (
          // eslint-disable-next-line react/jsx-key
          <IconField
            listKey={uid(ind)}
            icon={item.icon}
            placeholder={item.placeholder}
            label={item.label}
            type={item.type}
            name={item.name}
            control={control}
          />
        ))}
      </div>
      <S10Button text="Submit" styleType="gradientBackground" type="submit" />
    </form>
  );
};

export default BookingForm;
