import { FC } from "react";
import styles from "../../styles/Appointment.module.scss";
import type { Props } from "../../types/pages/appointment/DoctorsList";
import { contents } from "../../contents/doctor";
import { ConsultCard } from "../home/ConsultCard";

const DoctorsList: FC<Props> = ({ doctorsList }) => (
  <div className={styles.doctorsList}>
    {doctorsList.map((doc: { [key: string]: any }, index: number) => (
      // eslint-disable-next-line react/jsx-key
      <ConsultCard
        Listkey={index}
        image={contents.sampleDoctor.imageOne}
        doctor={doc}
      />
    ))}
  </div>
);

export default DoctorsList;
