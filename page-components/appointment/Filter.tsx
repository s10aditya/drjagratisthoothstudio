import { Slider, ToggleButton, ToggleButtonGroup } from "@mui/material";
import { FC, MouseEvent, useState } from "react";
import { uid } from "react-uid";
import styles from "../../styles/Appointment.module.scss";
import { contents } from "../../contents/appointment";

const Filter: FC = () => {
  const [isPaid, setIsPaid] = useState<string>("Free");
  const [priceRange, setPriceRange] = useState<number[]>([20, 37]);

  const handlePaidChange = (_: MouseEvent<HTMLElement>, newAlignment: string) =>
    setIsPaid(newAlignment);

  const handleRangeChange = (event: Event, newValue: number | number[]) =>
    setPriceRange(newValue as number[]);

  return (
    <div className={styles.filter__main}>
      <h6>{contents.filter.heading}</h6>
      <ToggleButtonGroup
        color="primary"
        value={isPaid}
        exclusive
        onChange={handlePaidChange}
      >
        {contents.filter.buttons.map((item, index) => (
          <ToggleButton key={uid(index)} value={item} size="medium">
            {item}
          </ToggleButton>
        ))}
      </ToggleButtonGroup>
      <h6>{contents.filter.rangeHeading}</h6>
      <Slider
        value={priceRange}
        onChange={handleRangeChange}
        valueLabelDisplay="auto"
      />
      <p>
        Price ${priceRange[0]} - ${priceRange[1]}
      </p>
    </div>
  );
};

export default Filter;
