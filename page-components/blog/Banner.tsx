/* eslint-disable jsx-a11y/alt-text */
import { FC } from "react";
import Image from "next/image";
import styles from "./Banner.module.scss";
import { contents } from "../../contents/blog";

export const Banner: FC = () => (
  <div className={styles.banner__main}>
    <Image {...contents.sampleBlog.imageOne} />
  </div>
);
