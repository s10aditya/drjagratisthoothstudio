import { FC } from "react";
import Image from "next/image";
import { contents } from "../../contents/blog";
import styles from "./Comments.module.scss";
import { useForm } from "react-hook-form";
import S10Button from "../../reusable-components/Button";

const CommentItem = ({
  description,
  date,
}: {
  description: string;
  date: string;
}) => (
  <div className={styles.comments__commentItem}>
    <div>
      <div className={styles.comments__userImg} />
    </div>

    <div className={styles.comments__userContent}>
      <p>{description}</p>
      <div className={styles.comments__userActions}>
        <span>{date}</span>
        <span>Reply</span>
      </div>
    </div>
  </div>
);

export const Comments: FC = () => {
  const { register, handleSubmit } = useForm();

  return (
    <div className={styles.comments__main}>
      <h5>
        {contents.comments.count} {contents.comments.header}
      </h5>
      <div className={styles.comments__commentBox}>
        <CommentItem
          description={contents.comments.sampleComment.description}
          date={contents.comments.sampleComment.date}
        />
      </div>
      <form
        onSubmit={handleSubmit((v) => console.log({ v }))}
        className={styles.comments__addComment}
      >
        <div>
          <h5>{contents.comments.comment.header}</h5>
          <p>{contents.comments.comment.description}</p>
        </div>
        <div className={styles.comments__inputFields}>
          <input
            {...register("name")}
            placeholder="Name"
            className={styles.commentInput}
          />
          <input
            {...register("email")}
            placeholder="Email"
            className={styles.commentInput}
          />
          <textarea
            {...register("comment")}
            placeholder="Enter your comments"
            className={styles.commentInput}
          />
        </div>
        <S10Button text="Submit" styleType="gradientBackground" />
      </form>
    </div>
  );
};
