import { FC, useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { uid } from "react-uid";
import { Tabs } from "../../reusable-components/Tabs";
import { contents } from "../../contents/blogs";
import styles from "./BlogCategory.module.scss";
import { Pagination } from "@mui/material";
import { styled } from "@mui/system";
import type { TBlogCategoryProps } from "../../types/pages/blogs/BlogCategory";
import { fetchData } from "../../apiHelpers";

const CustomPagination = styled(Pagination)({
  ".MuiPagination-ul": {
    "& > li": {
      "& > button": {
        backgroundColor: "#fff",
        color: "#535353",
        "&[aria-current=true]": {
          backgroundColor: "#FF8A00",
          color: "#FFF",
        },
      },
    },
  },
});

const BlogCategory: FC<TBlogCategoryProps> = () => {
  const [activeCategory, setActiveCategory] = useState(
    contents.blogCategories.categories[0].value
  );
  const [activePage, setActivePage] = useState(1);
  const [blogsList, setBlogsList] = useState<{ [key: string]: any }[]>([]);

  useEffect(() => {
    getAllBlogs(1);
  }, []);

  const getAllBlogs = async (page: number) => {
    try {
      const data = await fetchData("getOrganizationBlogs", true, {
        page,
        type: "all",
      });

      if (Array.isArray(data?.data)) {
        setBlogsList(data.data);
        setActivePage(page);
      }
    } catch (error) {}
  };

  return (
    <div className={styles.blogCategory__main}>
      <h5>{contents.blogCategories.header}</h5>
      <Tabs
        items={contents.blogCategories.categories}
        activeTab={activeCategory}
        onTabChange={(newTab) => setActiveCategory(newTab)}
      />
      <div className={styles.blogCategory__blogs}>
        {blogsList.map((blog, ind) => (
          <Link href={`/blog/${blog.blogId}`} passHref key={uid(ind)}>
            <div className={styles.blogCategory__blogCard}>
              {/* eslint-disable-next-line jsx-a11y/alt-text */}
              <Image
                src={blog.image}
                alt="blog-image"
                width="286"
                height="220"
              />
              <p>{blog.blog_title}</p>
            </div>
          </Link>
        ))}
      </div>
      <CustomPagination
        count={contents.blogCategories.blogs.length}
        color="primary"
        variant="outlined"
        size="large"
        showFirstButton
        showLastButton
        page={activePage}
        onChange={(_, p) => {
          getAllBlogs(p);
        }}
      />
    </div>
  );
};

export default BlogCategory;
