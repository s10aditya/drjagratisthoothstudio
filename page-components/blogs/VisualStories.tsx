import { FC, useEffect, useState } from "react";
import { uid } from "react-uid";
import Image from "next/image";
import { contents } from "../../contents/blogs";
import styles from "./VisualStories.module.scss";
import { fetchData } from "../../apiHelpers";

export const VisualStories: FC = () => {
  const [visualStoriesList, setVisualStoriesList] = useState<
    { [key: string]: any }[]
  >([]);

  useEffect(() => {
    getAllVisualStories(1);
  }, []);

  const getAllVisualStories = async (page: number) => {
    try {
      const data = await fetchData("getVisualStories");

      if (Array.isArray(data?.data)) {
        setVisualStoriesList(data.data);
      }
    } catch (error) {}
  };

  return (
    <div className={styles.visualStories__main}>
      <h5>{contents.visualStories.header}</h5>
      <div className={styles.visualStories__storiesContainer}>
        {visualStoriesList.map((story, ind) => {
          const imageProps = {
            ...contents.visualStories.stories[0].image,
            src: story.image,
          };

          return (
            <div key={uid(ind)} className={styles.visualStories__storyCard}>
              {/* eslint-disable-next-line jsx-a11y/alt-text */}
              <Image {...imageProps} />
              <p>{story.storyTitle}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};
