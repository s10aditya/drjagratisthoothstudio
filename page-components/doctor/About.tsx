import clsx from "clsx";
import { FC } from "react";
import { uid } from "react-uid";
import { contents } from "../../contents/doctor";
import styles from "./About.module.scss";

export const AboutAwards = ({ variant }: { variant: string }) => (
  <div
    className={clsx(styles.about__awards, {
      [styles.about__awardsGradient]: variant === "gradient",
    })}
  >
    {contents.about.sectionTwo.awards.map((award, ind) => (
      <div
        className={clsx(styles.awardItem, {
          [styles.awardItemGradient]: variant === "gradient",
        })}
        key={uid(ind)}
      >
        {award.svg({ variant })}
        <span className={styles.award__text}>{award.text1}</span>
        <p className={styles.award__label}>{award.label}</p>
      </div>
    ))}
  </div>
);

const About: FC = () => {
  return (
    <div className={styles.about__main}>
      <section>
        <h4>{contents.about.sectionOne.header}</h4>
        <p>{contents.about.sectionOne.description}</p>
      </section>
      <section className={styles.about__sectionTwo}>
        <h4>{contents.about.sectionTwo.header}</h4>
        <AboutAwards variant="standard" />
        <p>{contents.about.sectionTwo.description}</p>
      </section>
    </div>
  );
};

export default About;
