import { FC } from "react";
import styles from "./Banner.module.scss";

const Banner: FC = () => <div className={styles.bannerBG} />;

export default Banner;
