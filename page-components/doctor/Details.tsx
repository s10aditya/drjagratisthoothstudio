import { FC } from "react";
import Image from "next/image";
import styles from "./Details.module.scss";
import { contents } from "../../contents/doctor";
import { uid } from "react-uid";
import { TDetailsProps } from "../../types/pages/doctor/details";

const Details: FC<TDetailsProps> = ({}) => {
  return (
    <div className={styles.doctorDetails__main}>
      <div className={styles.doctorDetails__profilePicture}>
        {/* eslint-disable-next-line jsx-a11y/alt-text */}
        <Image {...contents.sampleDoctorSmall} />
      </div>
      <h3>{contents.header}</h3>
      <p className={styles.doctorDetails__qualification}>
        <span>{contents.qualifications[0]}</span>
        <span>{contents.qualifications[1]}</span>
      </p>
      <p className={styles.doctorDetails__clinicName}>{contents.clinicName}</p>
      <div className={styles.doctorDetails__blockOne}>
        <p className={styles.doctorDetails__clinicAddress}>
          {contents.clinicAddress}
        </p>
        <input type="date" className={styles.doctorDetails__appDate} />
      </div>
      <div className={styles.doctorDetails__rightButtons}>
        <button className={styles.doctorDetails__clinicalAppointment}>
          {contents.rightButtons[0]}
        </button>
        <button className={styles.doctorDetails__price}>
          {contents.rightButtons[1]}
        </button>
      </div>
      <div className={styles.doctorDetails__socialInterest}>
        <p className={styles.doctorDetails__noOfPeople}>
          {contents.socialInterest.peopleInterested}
        </p>
        <p className={styles.doctorDetails__socialInterestText}>
          {contents.socialInterest.text}
        </p>
        <div>
          {contents.socialMedias.map((item, ind) => (
            <span key={uid(ind)}>{item}</span>
          ))}
        </div>
      </div>
      <div className={styles.doctorDetails__timeSlots}>
        <p>{contents.timeSlots.header}</p>
        <div className={styles.doctorDetails__timings}>
          <span>{contents.timeSlots.am}</span>
          <div>
            {contents.timeSlots.availableTimeSlots.map((item, ind) => (
              <button key={uid(ind)}>{item}</button>
            ))}
          </div>
        </div>
        <div className={styles.doctorDetails__timings}>
          <span>{contents.timeSlots.pm}</span>
          <div>
            {contents.timeSlots.availableTimeSlots.map((item, ind) => (
              <button key={uid(ind)}>{item}</button>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Details;
