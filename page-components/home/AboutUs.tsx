import styles from "../../styles/Home.module.scss";
import Image from "next/image";
import S10Button from "../../reusable-components/Button";
import { FC } from "react";
import { aboutUs } from "../../contents/home";

export const AboutUs: FC = () => (
  <div className={styles.aboutUs__main}>
    <strong>
      <h4>{aboutUs.headerOne}</h4>
    </strong>
    <div className={styles.aboutUs__container}>
      <div className={styles.aboutUs__blockOne}>
        <div className={styles.aboutUs__clippedContainer}>
          <div
            style={{ paddingLeft: "2rem", width: "110%", marginTop: "0.5rem" }}
          >
            <Image {...aboutUs.image} height={700} />
          </div>
        </div>
        <br />
        <br />
        <div className={styles.aboutUs__blockTwo} style={{ padding: "10px" }}>
          <h5>{aboutUs.headerThree}</h5>
          <p>{aboutUs.descriptionThree}</p>
        </div>
        <br />
        <div className={styles.aboutUs__blockTwo} style={{ padding: "10px" }}>
          <h5>{aboutUs.headerFour}</h5>
          <p>{aboutUs.decriptionFour}</p>
        </div>
        <br />
        <div className={styles.aboutUs__blockTwo} style={{ padding: "10px" }}>
          <h5>{aboutUs.headerSix}</h5>
          <p>{aboutUs.descriptionSix}</p>
        </div>
      </div>
      <div className={styles.aboutUs__blockTwo} style={{ marginTop: "-10rem" }}>
        <h5>{aboutUs.headerTwo}</h5>
        <p>{aboutUs.description}</p>
        <p>{aboutUs.descriptionTwo}</p>
        <br />
        <h5>{aboutUs.headerFive}</h5>
        <p>{aboutUs.descriptionFive}</p>
        <br />
        <h5>{aboutUs.headerSeven}</h5>
        <p>{aboutUs.descriptionSeven}</p>
      </div>
    </div>
  </div>
);
