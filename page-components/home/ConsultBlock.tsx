import { FC } from "react";
import React, { useEffect, useState } from "react";
import styles from "../../styles/Home.module.scss";
import S10Button from "../../reusable-components/Button";
import { ConsultCard } from "./ConsultCard";
import { consultBlock } from "../../contents/home";
import { contents } from "../../contents/doctor";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
    slidesToSlide: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1,
  },
};

const ConsultBlock = () => {
  return (
    <div className={styles.consultBlock__main}>
      <h4 className={styles.consultBlock__headerOne}>
        {consultBlock.headerOne}
      </h4>
    </div>
  );
};

export default ConsultBlock;
