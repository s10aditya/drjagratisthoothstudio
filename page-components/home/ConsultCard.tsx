import Image from "next/image";
import Link from "next/link";
import { FC } from "react";
import styles from "../../styles/Home.module.scss";
import type { TConsultCardProps } from "../../types/pages/home";
import { consultCard } from "../../contents/home";
import { uid } from "react-uid";

export const ConsultCard: FC<TConsultCardProps> = ({
  Listkey,
  image,
  doctor,
}) => (
  <div className={styles.consultCard__main} key={uid(Listkey)}>
    <Image {...image} />
    <h5 style={{ color: "black" }}>{doctor?.docName}</h5>
  </div>
);
