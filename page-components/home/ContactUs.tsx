import S10Button from "../../reusable-components/Button";
import { MailIcon, PhoneIcon } from "../../SVGs";
import RoomOutlinedIcon from "@mui/icons-material/RoomOutlined";
import styles from "../../styles/Home.module.scss";
import { TContactUsForm } from "../../types/pages/home";
import { useForm, Controller } from "react-hook-form";
import { FC } from "react";
import { contactUs } from "../../contents/home";
import { TextField } from "@mui/material";

export const ContactUs: FC = () => {
  const { control, handleSubmit } = useForm<TContactUsForm>({
    mode: "onBlur",
  });

  return (
    <div className={styles.contactUs__main}>
      <div className={styles.contactUs__blockOne}>
        <h4>{contactUs.headerOne}</h4>
        <p>{contactUs.description}</p>
        <form onSubmit={handleSubmit((v) => console.log({ v }))}>
          <Controller
            name="name"
            control={control}
            render={({ field: { onChange, value } }) => (
              <TextField
                onChange={onChange}
                value={value}
                variant="standard"
                label="Name"
              />
            )}
          />
          <Controller
            name="emailId"
            control={control}
            render={({ field: { onChange, value } }) => (
              <TextField
                onChange={onChange}
                value={value}
                variant="standard"
                label="Email"
              />
            )}
          />
          <Controller
            name="phoneNumber"
            control={control}
            render={({ field: { onChange, value } }) => (
              <TextField
                onChange={onChange}
                value={value}
                variant="standard"
                label="Phone number"
              />
            )}
          />
          <Controller
            name="message"
            control={control}
            render={({ field: { onChange, value } }) => (
              <TextField
                onChange={onChange}
                value={value}
                variant="standard"
                label="Message"
              />
            )}
          />
          <S10Button
            text={contactUs.button.text}
            type="submit"
            styleType="gradientBackground"
          />
        </form>
      </div>

      <div className={styles.contactUs__blockTwo}>
        <h5>{contactUs.info.header}</h5>
        <div className={styles.contactInfoContainer}>
          <div>
            <PhoneIcon />
            {contactUs.info.phone}
          </div>
          <div>
            <RoomOutlinedIcon />
            {contactUs.info.address}
          </div>
        </div>
      </div>
    </div>
  );
};
