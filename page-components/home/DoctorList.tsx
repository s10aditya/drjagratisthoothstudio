import { FC } from "react";
import React, { useEffect, useState } from "react";
import styles from "../../styles/Home.module.scss";

import { ConsultCard } from "./ConsultCard";
import { consultBlock } from "../../contents/home";
import { contents } from "../../contents/doctor";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";

const responsive = {
  desktop: {
    breakpoint: { max: 3000, min: 1024 },
    items: 3,
    slidesToSlide: 3,
  },
  tablet: {
    breakpoint: { max: 1024, min: 464 },
    items: 2,
    slidesToSlide: 2,
  },
  mobile: {
    breakpoint: { max: 464, min: 0 },
    items: 1,
    slidesToSlide: 1,
  },
};

const ConsultBlock = () => {
  return (
    <Carousel
      responsive={responsive}
      infinite={true}
      transitionDuration={600}
      autoPlay={true}
      className={styles.consultCards__main}
    >
      <ConsultCard
        Listkey={0}
        image={contents.sampleDoctor.imageOne}
        doctor={{
          docName: contents.sampleDoctor.headerOne,
        }}
      />
      <ConsultCard
        Listkey={0}
        image={contents.sampleDoctor.imageTwo}
        doctor={{
          docName: contents.sampleDoctor.headerTwo,
        }}
      />
      <ConsultCard
        Listkey={0}
        image={contents.sampleDoctor.imageThree}
        doctor={{
          docName: contents.sampleDoctor.headerThree,
        }}
      />
      <ConsultCard
        Listkey={0}
        image={contents.sampleDoctor.imageFour}
        doctor={{
          docName: contents.sampleDoctor.headerFour,
        }}
      />
      <ConsultCard
        Listkey={0}
        image={contents.sampleDoctor.imageFive}
        doctor={{
          docName: contents.sampleDoctor.headerFive,
        }}
      />
      <ConsultCard
        Listkey={0}
        image={contents.sampleDoctor.imageSix}
        doctor={{
          docName: contents.sampleDoctor.headerSix,
        }}
      />
    </Carousel>
  );
};

export default ConsultBlock;
