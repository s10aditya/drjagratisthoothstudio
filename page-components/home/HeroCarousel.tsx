import { FC } from "react";
import Image from "next/image";
import styles from "./Carousel.module.scss";
import S10Button from "../../reusable-components/Button";
import { ArrowNext, ArrowPrev } from "../../SVGs";
import { heroCarousel } from "../../contents/home";

export const HeroCarousel: FC = () => {
  return (
    <div className={styles.slideOne}>
      <div className={styles.slideOneTextContainer}>
        <p className={styles.slideOneTextOne}>
          {heroCarousel.header.one}
          <span>{heroCarousel.header.two}</span>
        </p>
        <p className={styles.slideOneTextTwo}>{heroCarousel.header.three}</p>
        <S10Button text={heroCarousel.button.text} />
        <div className={styles.navigationButtons}>
          <button className={styles.prevButton}>
            <ArrowPrev />
          </button>
          <button className={styles.nextButton}>
            <ArrowNext />
          </button>
        </div>
      </div>
      <div className={styles.slideOneImageContainer}>
        {/* eslint-disable-next-line jsx-a11y/alt-text */}
        <Image {...heroCarousel.image} />
      </div>
    </div>
  );
};
