import { FC } from "react";
import Image from "next/image";
import { ourBlogs } from "../../contents/home";
import styles from "../../styles/Home.module.scss";
import blogOne from "../../public/Home/blogOne.jpg";
import blogTwo from "../../public/Home/blogTwo.jpg";
import blogThree from "../../public/Home/blogThree.jpg";
export const OurBlogs: FC = () => (
  <>
    <div className={styles.consultBlock__main}>
      <h4 className={styles.consultBlock__headerOne}>{ourBlogs.header}</h4>
    </div>
    <div className={styles.ourBlogs__main}>
      <Image src={blogOne} width={1000} height={1000} />
      <Image src={blogTwo} width={1000} height={1000} />
      <Image src={blogThree} width={1000} height={1000} />
    </div>
  </>
);
