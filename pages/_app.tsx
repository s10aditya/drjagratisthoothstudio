import "../styles/globals.css";

import type { AppProps } from "next/app";
import { ThemeProvider } from "@mui/material";
import { theme } from "../material-ui-theme";
import { Header } from "../page-components/Header";
import { Footer } from "../page-components/Footer";

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme}>
      <Header />
      <Component {...pageProps} />
      <Footer />
    </ThemeProvider>
  );
}

export default MyApp;
