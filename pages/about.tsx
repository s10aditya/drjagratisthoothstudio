import { Fade } from "@mui/material";
import { CustomerSupport } from "../page-components/about/CustomerSupport";
import { AboutAwards } from "../page-components/doctor/About";
import { AboutUs } from "../page-components/home/AboutUs";

function About() {
  return (
    <Fade in>
      <main>
        <AboutUs />
        <AboutAwards variant="gradient" />
        <CustomerSupport />
      </main>
    </Fade>
  );
}

export default About;
