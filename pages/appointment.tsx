import { Fade, MenuItem, Select, SelectChangeEvent } from "@mui/material";
import SearchIcon from "@mui/icons-material/Search";
import CloseRoundedIcon from "@mui/icons-material/CloseRounded";
import { styled } from "@mui/system";
import clsx from "clsx";
import { useState } from "react";
import { uid } from "react-uid";
import styles from "../styles/Appointment.module.scss";
import { contents } from "../contents/appointment";
import S10Button from "../reusable-components/Button";
import DoctorsList from "../page-components/appointment/DoctorsList";
import Filter from "../page-components/appointment/Filter";
import BookingForm from "../page-components/appointment/BookingForm";
import { Tabs } from "../reusable-components/Tabs";
import { TActiveTab, TStrUnkown } from "../types/pages/appointment";
import { fetchData } from "../apiHelpers";
import { GetStaticProps, InferGetStaticPropsType } from "next";

const FilterSelect = styled(Select)({
  borderRadius: 10,
  fieldset: {
    border: 0,
  },
  ".MuiSelect-select": {
    backgroundColor: "#F0F0F0",
  },
  ".MuiSelect-icon": {
    fontSize: "2rem",
    right: 0,
    color: "#000",
  },
});

function Appointment({
  doctorsList,
}: InferGetStaticPropsType<typeof getStaticProps>) {
  const [activeTab, setActiveTab] = useState<TActiveTab>(
    contents.tabItems[0].value
  );
  const [activeFilter, setActiveFilter] = useState<TStrUnkown>(
    contents.filterItems[0].value
  );

  const handleTabChange = (newTab: TActiveTab) => {
    if (newTab !== activeTab) {
      setActiveTab(newTab);
    }
  };

  const handleFilterChange = (event: SelectChangeEvent<TStrUnkown>) => {
    if (event.target.value !== activeFilter) {
      setActiveFilter(event.target.value);
    }
  };

  return (
    <main className={styles.appointment__container}>
      <section>
        <h4>{contents.header}</h4>
        <Tabs
          activeTab={activeTab}
          onTabChange={handleTabChange}
          items={contents.tabItems}
        />
        <Fade
          in={activeTab === contents.tabItems[0].value}
          mountOnEnter
          unmountOnExit
        >
          <div>
            <div className={styles.appointment__filterContainer}>
              <FilterSelect
                labelId="doctors-filter"
                value={activeFilter}
                onChange={handleFilterChange}
                size="small"
              >
                {contents.filterItems.map((item, ind) => (
                  <MenuItem key={uid(ind)} value={item.value}>
                    {item.label}
                  </MenuItem>
                ))}
              </FilterSelect>
              <div className={styles.appointment__searchContainer}>
                <SearchIcon />
                <input
                  className={styles.searchInput}
                  placeholder={contents.searchPlaceholder}
                />
                <button className={styles.closeButton}>
                  <CloseRoundedIcon />
                </button>
              </div>
              <S10Button
                text={contents.filterSearch}
                styleType="gradientBackground"
                size="small"
              />
            </div>
            <div className={styles.appointment__doctorsContainer}>
              <DoctorsList doctorsList={doctorsList} />
              <Filter />
            </div>
          </div>
        </Fade>
        <Fade
          in={activeTab === contents.tabItems[1].value}
          mountOnEnter
          unmountOnExit
        >
          <div className={styles.appointment__clinicalContainer}>
            <div className={styles.appointment__clinicalBlockOne}>
              <h3>{contents.clinical.booking.header}</h3>
              <p>{contents.clinical.booking.description}</p>
              <BookingForm />
            </div>
            <div className={styles.appointment__clinicalBlockTwo}>
              <h3>{contents.clinical.timings.header}</h3>
              <div>
                {contents.clinical.timings.list.map((item, index) => (
                  <div
                    key={uid(index)}
                    className={clsx(styles.timing, {
                      [styles.timingMB]:
                        index < contents.clinical.timings.list.length - 1,
                    })}
                  >
                    {item}
                  </div>
                ))}
              </div>
            </div>
          </div>
        </Fade>
      </section>
    </main>
  );
}

export default Appointment;

export const getStaticProps: GetStaticProps = async () => {
  let res;
  try {
    res = await fetchData("getDoctorsList");
  } catch (error) {
    res = { data: { data: [] } };
  }
  return {
    props: {
      doctorsList: res?.data?.data,
    },
    revalidate: 60 * 60 * 60,
  };
};
