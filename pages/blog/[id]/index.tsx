/* eslint-disable jsx-a11y/alt-text */
import Image from "next/image";
import { Fade } from "@mui/material";
import { Banner } from "../../../page-components/blog/Banner";
import styles from "../../../styles/Blog.module.scss";
import { contents } from "../../../contents/blog";
import { uid } from "react-uid";
import { Comments } from "../../../page-components/blog/Comments";
import { GetStaticPaths, GetStaticProps } from "next";
import { fetchData } from "../../../apiHelpers";

function Blog() {
  return (
    <Fade in mountOnEnter unmountOnExit>
      <div className={styles.blog__main}>
        <Banner />
        <div className={styles.blog__content}>
          <p>{contents.sampleBlog.description.one}</p>
          <h6>{contents.sampleBlog.subHeading}</h6>
          <p>{contents.sampleBlog.description.two}</p>
          <div className={styles.blog__image}>
            <Image {...contents.sampleBlog.imageTwo} />
          </div>
          <p>{contents.sampleBlog.description.two}</p>
          <div className={styles.blog__tagsAndShare}>
            <div className={styles.blog__tags}>
              <span>{contents.sampleBlog.tags.header}</span>
              <div>
                {contents.sampleBlog.tags.list.map((tag, ind) => (
                  <span key={uid(ind)}>{`${tag}${
                    ind !== contents.sampleBlog.tags.list.length - 1 ? ", " : ""
                  }`}</span>
                ))}
              </div>
            </div>
            <div className={styles.blog__shareWith}>
              <span>{contents.social.header}</span>
              <div>
                {contents.social.list.map(({ icon }, ind) => (
                  <span key={uid(ind)}>{icon}</span>
                ))}
              </div>
            </div>
          </div>
          <div className={styles.blog__otherBlogs}>
            <div>
              <h6>{contents.otherBlogs.prev.title}</h6>
              <span>{contents.otherBlogs.prev.cap}</span>
            </div>
            <div>
              <h6>{contents.otherBlogs.next.title}</h6>
              <span>{contents.otherBlogs.next.cap}</span>
            </div>
          </div>
        </div>
        <Comments />
      </div>
    </Fade>
  );
}

export default Blog;

export const getStaticPaths: GetStaticPaths = async () => {
  let paths = [];
  try {
    const res = await fetchData("getOrganizationBlogs");

    paths = res.data.data.map((datum: { [key: string]: any }) => ({
      params: { id: datum.blogId },
    }));
  } catch (error) {}

  return {
    paths,
    fallback: false,
  };
};

export const getStaticProps: GetStaticProps = async ({ params }) => {
  try {
    const blog = await fetchData("viewBlogs", true, { blogId: params?.id });
    return { props: { blog } };
  } catch (error) {
    return { props: { blog: [] } };
  }
};
