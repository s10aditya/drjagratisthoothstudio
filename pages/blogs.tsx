import { Fade } from "@mui/material";
import styles from "../styles/Blogs.module.scss";
import BlogCategory from "../page-components/blogs/BlogCategory";
import { VisualStories } from "../page-components/blogs/VisualStories";

function Blogs() {
  return (
    <Fade in>
      <main className={styles.blogs__main}>
        <VisualStories />
        <BlogCategory />
      </main>
    </Fade>
  );
}

export default Blogs;
