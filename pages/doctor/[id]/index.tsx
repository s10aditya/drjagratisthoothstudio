import { Fade } from "@mui/material";
import { useRouter } from "next/router";
import styles from "../../../styles/Doctor.module.scss";
import About from "../../../page-components/doctor/About";
import Banner from "../../../page-components/doctor/Banner";
import Details from "../../../page-components/doctor/Details";

function Doctor() {
  const router = useRouter();
  const { id } = router.query;

  return (
    <Fade in mountOnEnter unmountOnExit>
      <div className={styles.doctor__main}>
        <Banner />
        <Details />
        <About />
      </div>
    </Fade>
  );
}

export default Doctor;
