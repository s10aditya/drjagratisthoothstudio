import React from "react";
import ConsultBlock from "../page-components/home/ConsultBlock";
import { OurBlogs } from "../page-components/home/OurBlogs";
import { AboutUs } from "../page-components/home/AboutUs";
import { HeroCarousel } from "../page-components/home/HeroCarousel";
import styles from "../styles/Home.module.scss";
import { Fade } from "@mui/material";
import { ContactUs } from "../page-components/home/ContactUs";
import DoctorList from "../page-components/home/DoctorList";
function HomePage() {
  return (
    <Fade in>
      <main className={styles.home__main}>
        <HeroCarousel />
        <ConsultBlock />
        <DoctorList />
        <AboutUs />
        <OurBlogs />
        <ContactUs />
      </main>
    </Fade>
  );
}

export default HomePage;
