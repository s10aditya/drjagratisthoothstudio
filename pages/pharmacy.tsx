import {
  Badge,
  Checkbox,
  FormControlLabel,
  Rating,
  Slider,
} from "@mui/material";
import React, { useState } from "react";
import { uid } from "react-uid";
import S10Button from "../reusable-components/Button";
import styles from "../styles/Pharmacy.module.scss";
import { contents } from "../contents/pharmacy";

function Pharmacy() {
  const [priceRange, setPriceRange] = useState<number[]>([20, 37]);

  const handleRangeChange = (event: Event, newValue: number | number[]) =>
    setPriceRange(newValue as number[]);

  return (
    <main className={styles.pharmacy__main}>
      <h1>{contents.header}</h1>
      <nav>
        {contents.sidebar.items.map((item, index) => (
          <div key={uid(index)}>
            <h6>{item.title}</h6>
            <ul>
              {item.content.map((datum, ind) => (
                <li key={uid(ind)} className={styles.pharmacy__addFlex}>
                  {item.title === "Category" ? (
                    <>
                      {datum}
                      <Badge badgeContent={4} color="primary" />
                    </>
                  ) : item.title === "Brand" ? (
                    <FormControlLabel
                      sx={{ margin: 0, gap: "1.2rem" }}
                      control={<Checkbox color="primary" sx={{ padding: 0 }} />}
                      label={datum}
                    />
                  ) : (
                    <>
                      <Checkbox color="primary" sx={{ padding: 0 }} />
                      {typeof datum === "number" && (
                        <Rating value={datum} readOnly />
                      )}
                    </>
                  )}
                </li>
              ))}
            </ul>
          </div>
        ))}

        <div>
          <h6>{contents.sidebar.other.title}</h6>
          <Slider
            value={priceRange}
            onChange={handleRangeChange}
            valueLabelDisplay="auto"
          />
          <div className={styles.pharmacy__minMaxContainer}>
            {contents.sidebar.other.content.map((datum, ind) => (
              <label key={uid(ind)}>
                {datum}
                <input type="number" />
              </label>
            ))}
          </div>
        </div>
        <div className={styles.pharmacy__buttons}>
          <S10Button {...contents.sidebar.other.buttons[0]} />
          <S10Button {...contents.sidebar.other.buttons[1]} />
        </div>
      </nav>
    </main>
  );
}

export default Pharmacy;
