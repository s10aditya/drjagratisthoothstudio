import clsx from "clsx";
import { FC } from "react";
import styles from "./Button.module.scss";

interface Props {
  text: string;
  onClick?: () => void;
  disabled?: boolean;
  type?: "button" | "submit" | "reset" | undefined;
  styleType?: string;
  size?: "small" | "large" | string;
}

const S10Button: FC<Props> = ({
  text,
  onClick,
  disabled,
  type,
  styleType,
  size,
}) => (
  <button
    onClick={onClick}
    disabled={disabled}
    type={type}
    className={clsx(styles.buttonMain, {
      [styles.gradientBackground]: styleType === "gradientBackground",
      [styles.outlined]: styleType === "outlined",
      [styles.small]: size === "small",
    })}
    role="button"
  >
    <span
      className={clsx({
        [styles.gradientContainer]:
          !styleType || styleType === "gradientContainer",
      })}
    >
      {text}
    </span>
  </button>
);

export default S10Button;
