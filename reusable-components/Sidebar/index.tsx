import { uid } from "react-uid";
import type { TSidebarProps } from "../../types/components/Sidebar";
import styles from "./Sidebar.module.scss";

function Sidebar({ isOpen, onClose, items }: TSidebarProps) {
  return (
    <nav className={styles.sidebar__main}>
      <ul>
        {items.map((item, ind) => (
          <li key={uid(ind)} className={styles.sidebar__itemOne}>
            {item.label}
            {item.value.hasOwnProperty("list") && (
              <ul>
                {item.value.list.map((item: string, ind: number) => (
                  <li key={uid(ind)} className={styles.sidebar__itemTwo}>
                    {item}
                  </li>
                ))}
              </ul>
            )}
          </li>
        ))}
      </ul>
    </nav>
  );
}

export default Sidebar;
