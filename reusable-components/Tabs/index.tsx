import clsx from "clsx";
import { FC } from "react";
import { uid } from "react-uid";
import styles from "./Tabs.module.scss";

interface Props {
  items: {
    label: string;
    value: string;
  }[];
  onTabChange: (newTab: string) => void;
  activeTab: string;
}

export const Tabs: FC<Props> = ({ items, onTabChange, activeTab }) => (
  <div className={styles.Tabs__main}>
    {items.map((item, ind) => (
      <div
        onClick={() => onTabChange(item.value)}
        key={uid(ind)}
        className={clsx(styles.Tabs__tabItem, {
          [styles.Tabs__tabItemActive]: activeTab === item.value,
        })}
      >
        {item.label}
      </div>
    ))}
  </div>
);
