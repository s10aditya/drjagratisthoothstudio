export interface TSidebarProps {
  isOpen: boolean;
  onClose: () => void;
  items: { [key: string]: any }[];
}
