import { ReactNode } from "react";
import type { Control, FieldValues } from "react-hook-form";

export type TBookingFormItem = {
  icon: ReactNode;
  label: string;
  placeholder: string;
  type: string;
  name: string;
  control: Control<FieldValues, {}>;
  listKey: string;
};

export type TStrUnkown = string | unknown;
export type TActiveTab = string;
