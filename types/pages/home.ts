import { FC } from "react";

export type ReactFC = FC;

export type TConsultCardProps = {
  Listkey: number | string;
  image: {
    src: string;
    alt: string;
    width: number | string;
    height: number | string;
  };
  doctor: { [key: string]: any };
};

export interface TContactUsForm {
  name: string;
  emailId: string;
  phoneNumber: number;
  message: string;
}
